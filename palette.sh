#!/usr/bin/bash

for N in {0..16..1}; do
  SPC=''
  if [ $N -lt "10" ]; then
    SPC=' '
  fi
  echo -e "COLOUR ${SPC}${N} \e[38;5;${N}m█████ EXAMPLE TEXT: text1234 @!#*&\e[0m"
done
